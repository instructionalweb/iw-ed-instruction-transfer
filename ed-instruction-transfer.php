<?php
/*
Plugin Name: IW Ed Instruction Transfer
Plugin URI: https://instruction.austincc.edu/
Description: 
Version: 1.0
Author: Shea Scott
Author URI: 
License: GPL2
*/

function plugin_enqueue_scripts() {   
    $pluginUri = plugin_dir_url( __FILE__ );
    wp_enqueue_script( 'axios', 'https://cdnjs.cloudflare.com/ajax/libs/axios/0.21.1/axios.min.js', null, null , true );
    wp_enqueue_script( 'eit_transfer', $pluginUri . 'js/transfer.js', null, null,  true );
}

add_action( 'template_redirect', 'eit_plugin_is_page' );

function eit_plugin_is_page() {
    if ( is_page( 'ed-instruction-transfer' ) ) {
        add_action('wp_enqueue_scripts', 'plugin_enqueue_scripts');
    }
}

function ed_instruction_transfer_plugin($atts) {

    $Content = <<<EOD
        <style>
            main#main {
                background: url(https://instruction.austincc.edu/devtest/wp-content/uploads/sites/15/2022/08/SAC_@night.jpeg);
                background-repeat: no-repeat;
                background-size: cover;
                background-position: center;
            }
            main#main > article.post-type-page > section.article-content div.wrapper {
                background-color: white;
                padding: 1.25rem;
            }
            h2#ed-transfer-prompt {
                text-align: center;
            }
            div#compare-box {
                display: flex;
                flex-direction: column;
                justify-content: center;
                align-items: center;
                padding-top: 0.4rem;
            }
            div#compare-box label {
                font-size: 16px;
                font-weight: bold;
            }
            div#compare-box select {
                margin-bottom: 1.4rem;
                margin-top: 1rem;
                width: 90%;
            }
            div#et-container {
                width: 100% !important;
                overflow-x: auto;
            }

            table#compare { 
                display: none;
                margin-top: 1rem;
                font-size: 13px;
            }
            table#compare tr:nth-child(2) {
                text-align: center;
            }
            table#compare tr:nth-child(even) {
                background: #e8e7ea;
            }
            table#compare tr:nth-child(odd) {
                background: #cfcad6;
            }
            table#compare th {
                color: #fff;
                background-color: rgb(77, 25, 121) ;
                border-right: 1px solid #fff;
                border-bottom: 4px solid #fff;
            }
            table#compare th:last-child {
                border-right: none;
            }
            table#compare td {
                margin: 0;
                padding: 0.4rem;
                width: 50%;
                border-right: 1px solid #fff;
                border-bottom: 1px solid #fff;
            }
            table#compare td:last-child {
                border-right: none;
            }
            table#compare td:nth-child(1) {
                font-weight: bold;
            }

            .button-9 {
            appearance: button;
            backface-visibility: hidden;
            background-color: rgb(77, 25, 121);
            border-radius: 6px;
            border-width: 0;
            box-shadow: rgba(50, 50, 93, .1) 0 0 0 1px inset,rgba(50, 50, 93, .1) 0 2px 5px 0,rgba(0, 0, 0, .07) 0 1px 1px 0;
            box-sizing: border-box;
            color: #fff;
            cursor: pointer;
            font-family: -apple-system,system-ui,"Segoe UI",Roboto,"Helvetica Neue",Ubuntu,sans-serif;
            font-size: 100%;
            height: 44px;
            line-height: 1.15;
            outline: none;
            overflow: hidden;
            padding: 0 25px;
            position: relative;
            text-align: center;
            text-transform: none;
            transform: translateZ(0);
            transition: all .2s,box-shadow .08s ease-in;
            user-select: none;
            -webkit-user-select: none;
            touch-action: manipulation;
            width: 140px;
            }

            .button-9:disabled {
            cursor: default;
            }

            .button-9:focus {
            box-shadow: rgba(50, 50, 93, .1) 0 0 0 1px inset, rgba(50, 50, 93, .2) 0 6px 15px 0, rgba(0, 0, 0, .1) 0 2px 2px 0, rgba(50, 151, 211, .3) 0 0 0 4px;
            }
        </style>
        <h2 id="ed-transfer-prompt">How will your Associate of Arts in Teaching credits transfer?</h2>
        <div id="compare-box">
            <label for="school-select">Select a University</label>
            <select name="schools" id="school-select">
                <option id="compare-opt" value="null">--- Select an Option ---</option>
                <option value="all">All Universities</option>
            </select>
        </div>
        <div id="et-container">
            <table id="compare"></table>
        </div>
        
    EOD;
    
    return $Content;
}

add_shortcode('ed-transfer', 'ed_instruction_transfer_plugin');

    // <button role="button" class="button-9" id="compare-btn">Compare</button>
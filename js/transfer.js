document.addEventListener('DOMContentLoaded', function () {
  console.log('HELLO')
  const mainHeading = document.getElementsByTagName('h1')[1]
  console.log('mainHeading :>> ', mainHeading)
  mainHeading.style.display = 'none'
})

var compareBtn = document.getElementById('compare-btn')
var compareOption = document.getElementById('compare-opt')
var compareTable = document.getElementById('compare')
// compareBtn.addEventListener("click", function (e) {
//   compareTable.style.display = "table";
//   compareBtn.style.display = "none";
// });
// function populateColumn2(schoolNumber) {
//   for (var i = 0, row; (row = table.rows[i]); i++) {
//     console.log(rows[i].content.$t.split(","));
//     const otherSchool = rows[i].content.$t
//       .split(",")
//       [schoolNumber].split(": ")[1];
//     row.cells[1].textContent = otherSchool;
//   }
// }

function clearTable() {
  const headCells = table.getElementsByTagName('th')
  const bodyCells = table.getElementsByTagName('td')
  for (i = 0; i < bodyCells.length; i++) {
    if (i < headCells.length) {
      headCells[i].style.display = 'none'
    }
    bodyCells[i].style.display = 'none'
  }
}

function showTable() {
  const headCells = table.getElementsByTagName('th')
  const bodyCells = table.getElementsByTagName('td')
  for (i = 0; i < bodyCells.length; i++) {
    if (i < headCells.length) {
      headCells[i].style.display = 'table-cell'
    }
    bodyCells[i].style.display = 'table-cell'
  }
}

function showOneColumn(optionVal) {
  clearTable()
  const columnToShow = parseInt(optionVal) + 1

  console.log('COL ', columnToShow)
  const headCells = table.getElementsByTagName('th')
  const numOfCols = headCells.length
  const bodyCells = table.getElementsByTagName('td')
  for (i = 0; i < bodyCells.length; i += numOfCols) {
    if (i < numOfCols) {
      headCells[i].style.display = 'table-cell'
      headCells[i + columnToShow].style.display = 'table-cell'
    }
    bodyCells[i].style.display = 'table-cell'
    bodyCells[i + columnToShow].style.display = 'table-cell'
  }
}
function changeView(optionVal) {
  compareTable.style.display = 'table'
  compareOption.style.display = 'none'
  clearTable()
  if (optionVal === 'all') {
    showTable()
  } else {
    showOneColumn(optionVal)
  }
}

let rows = []
const table = document.getElementById('compare')
let acc = []
const sheetId = '1WGupnEyUj24pOMQQZNRL10IoQ2hJeL4uL47mhjDPqm8'
const apiKey = 'AIzaSyBJU4PeKulVbFBMrGUm9lSN3FwIdTQZQaE'

const data = axios
  .get(
    `https://sheets.googleapis.com/v4/spreadsheets/${sheetId}/values/Sheet1?key=${apiKey}`
  )
  .then((response) => {
    rows = response.data.values
    rows.shift()
    console.log(rows)
    // acc = rows.map((row) => row.title.$t);
    // console.log(acc);

    rows.forEach((el, i) => {
      const row = document.createElement('tr')
      if (i === 0) {
        el.forEach((cell) => {
          const th = document.createElement('th')
          th.textContent = cell
          row.appendChild(th)
        })
      } else if (i === 1) {
        el.forEach((cell) => {
          const th = document.createElement('td')
          const a = document.createElement('a')
          a.target = '_blank'
          a.href = cell
          a.innerText = 'Link'
          th.appendChild(a)
          row.appendChild(th)
        })
      } else {
        el.forEach((cell) => {
          const th = document.createElement('td')
          th.textContent = cell
          row.appendChild(th)
        })
      }
      table.appendChild(row)
      //   const col1 = document.createElement("td");
      //   col1.textContent = el;
      //   row.appendChild(col1);
      //   const col2 = document.createElement("td");
      //   row.appendChild(col2);
    })

    // Create options for <select>
    const select = document.getElementById('school-select')
    let universities = rows[0] //.map((s) => s.split(": ")[1]); // .filter( (val, i) => i % 2 !== 0 );
    universities.shift()
    universities.forEach((uni, i) => {
      //   console.log("UNI", uni);
      const option = document.createElement('option')
      option.textContent = uni
      option.setAttribute('value', i)
      select.appendChild(option)
    })

    select.addEventListener('change', (event) => {
      changeView(event.target.value)
    })

    console.log(universities)
  })
